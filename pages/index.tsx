import Link from 'next/link'

export default function Example() {
  return (
    <div className='bg-white'>
      <div className='md:b-12 mx-auto max-w-7xl px-4 pb-8 md:px-6 lg:px-8'>
        <div className='text-center'>
          <h2 className='text-base font-semibold uppercase tracking-wide text-amber-900'>
            ahead of the weather curve
          </h2>
          <p className='mt-1 text-4xl font-extrabold text-gray-900 md:text-5xl md:tracking-tight lg:text-6xl'>
            RouteWx
          </p>
          <img
            alt='RouteWx logo'
            className='mx-auto h-52 w-48 rounded-full py-4'
            src='https://www.nps.gov/common/uploads/grid_builder/romo/crop16_9/447276DE-AB73-13C5-6975E7C6E1BF5BFB.jpg?width=465&quality=90&mode=crop'
          />

          <div className='mx-auto mt-6 px-4 md:px-6 lg:px-8'>
            <div className='mx-auto max-w-4xl text-center'>
              <h2 className='text-3xl font-extrabold text-gray-900 md:text-4xl'>
                Plan your roadtrip with the optimal time to leave, based on
                current and forecasted weather conditions
              </h2>
              <p className='mt-3 text-xl text-gray-500 md:mt-4'>
                Powered by Swift Weather Solutions
              </p>
            </div>
          </div>
          <div className='mt-10 bg-white pb-12 md:pb-16'>
            <div className='relative'>
              <div className='relative mx-auto max-w-7xl px-4 md:px-6 lg:px-8'>
                <div className='mx-auto max-w-4xl'>
                  <dl className='items-end rounded-lg bg-white shadow-lg md:grid md:grid-cols-4'>
                    <div className='flex flex-col border-b border-gray-100 p-6 text-center md:border-0 md:border-r'>
                      <dt className='order-2 mt-2 text-lg font-medium leading-6 text-gray-500'>
                        <div className='inline-block translate-y-0 transform overflow-hidden rounded-lg bg-white px-4 pt-5 pb-4 text-left align-bottom opacity-100 shadow-xl transition-all sm:my-8 sm:w-full sm:max-w-sm sm:scale-100 sm:p-6 sm:align-middle'>
                          <div>
                            <div className='mt-3 text-center sm:mt-5'>
                              <h3
                                className='text-lg font-medium leading-6 text-gray-900'
                                id='headlessui-dialog-title-:r5:'
                              >
                                RouteWx Travel Guidance
                              </h3>
                              <div className='mt-2'>
                                <h1 style={{ color: 'orange' }}>
                                  Moderate Hazard Wind/Precipitation Possible.
                                </h1>
                                <h1 style={{ fontStyle: 'italic' }}>
                                  Optimal Departure Time:
                                </h1>
                                <h2>01/27/2023 1:16 PM</h2>
                                <h3 style={{ fontStyle: 'italic' }}>
                                  Departing Between:
                                </h3>
                                <h3>
                                  01/27/2023 11:15 AM &amp; 01/27/2023 2:15 PM
                                </h3>
                                <p className='text-sm text-gray-500'>
                                  Estimated Travel Time: 4 hours, 46 minutes.
                                  with 10 percent of the route impacted by
                                  weather, which could add 5 minutes to your
                                  travel time. Tap Route for forecast details.
                                </p>
                              </div>
                            </div>
                          </div>
                          <div className='mt-5 sm:mt-6'>
                            <button
                              type='button'
                              className='inline-flex w-full justify-center rounded-md border border-transparent bg-indigo-600 px-4 py-2 text-base font-medium text-white shadow-sm hover:bg-indigo-700 sm:text-sm'
                            >
                              Continue to map
                            </button>
                          </div>
                        </div>
                      </dt>
                      <dd className='order-1 text-5xl font-extrabold text-amber-900'>
                        When should I leave?
                      </dd>
                    </div>
                    <div className='flex flex-col border-t border-b border-gray-100 p-6 text-center md:border-0 md:border-l md:border-r'>
                      <dt className='order-2 mt-2 text-lg font-medium leading-6 text-gray-500'>
                        <div className='mapboxgl-popup-content'>
                          <div>
                            <h2
                              style={{
                                borderRadius: '10px',
                                textAlign: 'center',
                                margin: '5px',
                                color: 'white',
                                backgroundColor: 'palevioletred',
                              }}
                              className='font-lg'
                            >
                              Chance of freezing precip
                            </h2>
                            <div>
                              Forecast for 🕰️ :{' '}
                              <p style={{ fontWeight: '600' }}>
                                01/27/2023 12:03 MST
                              </p>
                            </div>
                            <div>
                              <p style={{ display: 'inline-block' }}>
                                Temperature 🌡️ :{' '}
                              </p>
                              <p
                                style={{
                                  fontWeight: '100',
                                  display: 'inline-block',
                                }}
                              >
                                &nbsp;36°F
                              </p>
                            </div>
                            <div>
                              <p style={{ display: 'inline-block' }}>
                                Precipitation ☔️ :{' '}
                              </p>
                              <p
                                style={{
                                  display: 'inline-block',
                                  fontWeight: '100',
                                }}
                              >
                                &nbsp;0.03 inches
                              </p>
                            </div>
                            <div style={{ backgroundColor: 'pink' }}>
                              <p style={{ display: 'inline-block' }}>
                                Frozen Precipitation 🌨️ :{' '}
                              </p>
                              <p
                                style={{
                                  fontWeight: '100',
                                  display: 'inline-block',
                                }}
                              >
                                &nbsp;22%
                              </p>
                            </div>
                            <div>
                              <p style={{ display: 'inline-block' }}>
                                Wind Gusts 🌬️ :{' '}
                              </p>
                              <p
                                style={{
                                  display: 'inline-block',
                                  fontWeight: '100',
                                }}
                              >
                                &nbsp;10.2 mph
                              </p>
                            </div>
                            <div>
                              <p style={{ display: 'inline-block' }}>
                                Visibility 🌤 :{' '}
                              </p>{' '}
                              <p
                                style={{
                                  display: 'inline-block',
                                  fontWeight: '100',
                                }}
                              >
                                &nbsp;3.60 miles
                              </p>
                            </div>
                          </div>
                          <button
                            className='mapboxgl-popup-close-button'
                            type='button'
                            aria-label='Close popup'
                            aria-hidden='true'
                          >
                            ×
                          </button>
                        </div>{' '}
                      </dt>
                      <dd className='order-1 text-5xl font-extrabold text-amber-900'>
                        Is there a chance of slick roads?
                      </dd>
                    </div>
                    <div className='flex flex-col border-t border-gray-100 p-6 text-center md:border-0 md:border-l'>
                      <dt className='order-2 mt-2 text-lg font-medium leading-6 text-gray-500'>
                        Number of forecast points per hour from a{' '}
                        <a href='https://microsoft.github.io/AIforEarthDataSets/data/noaa-hrrr.html'>
                          <u>high-res forecast model</u>
                        </a>
                      </dt>
                      <dd className='order-1 text-5xl font-extrabold text-amber-900'>
                        1.5 million
                      </dd>
                    </div>
                    <div className='flex flex-col border-t border-gray-100 p-6 text-center md:border-0 md:border-l'>
                      <dt className='order-2 mt-2 text-lg font-medium leading-6 text-gray-500'>
                        from the EPA{' '}
                        <a
                          className='font-bold italic hover:text-amber-700'
                          href='https://www.airnow.gov'
                        >
                          <u>AirNow</u>
                        </a>{' '}
                        API
                      </dt>
                      <dd className='order-1 text-5xl font-extrabold text-amber-900'>
                        Air Quality
                      </dd>
                    </div>
                  </dl>
                </div>
              </div>
            </div>
          </div>

          <h2 className='mt-4 text-3xl font-extrabold tracking-tight text-gray-900 md:text-4xl'>
            <span className='block'>Ready to dive in?</span>
            <span className='block'>Try out the demo today.</span>
          </h2>
          <div className='mt-8 flex justify-center'>
            <div className='inline-flex rounded-md shadow'>
              <Link
                href='/demo'
                className='inline-flex items-center justify-center rounded-md border border-transparent bg-amber-700 px-5 py-3 text-base font-medium text-white hover:bg-green-700'
              >
                Go to demo
              </Link>
            </div>
            {/* <div className='ml-3 inline-flex'>
              <a
                href='#'
                className='inline-flex items-center justify-center rounded-md border border-transparent bg-indigo-100 px-5 py-3 text-base font-medium text-indigo-700 hover:bg-indigo-200'
              >
                Learn more
              </a>
            </div> */}
          </div>
        </div>
      </div>
    </div>
  )
}
