import JSZip from 'jszip'
import { kml } from '@tmcw/togeojson'

// load the environment variable with our keys
const key = process.env['NEXT_PUBLIC_OPTIMIZE_API_KEY']
if (!key) {
  throw new Error(
    'The $NEXT_PUBLIC_OPTIMIZE_API_KEY environment variable was not found!'
  )
}
function addMinutes(numOfMinutes: number, date = new Date()) {
  date.setMinutes(date.getMinutes() + numOfMinutes)

  return date
}

export async function fetchWWA() {
  const zip = new JSZip()

  const x = await fetch(
    'https://routewx.s3.us-east-1.amazonaws.com/wwa/wwa.kmz'
  ) // 1) fetch the url
    .then(function (response) {
      // 2) filter on 200 OK
      if (response.status === 200 || response.status === 0) {
        return Promise.resolve(response.blob())
      } else {
        return Promise.reject(new Error(response.statusText))
      }
    })
    .then(JSZip.loadAsync) // 3) chain with the zip promise
    .then(function (zip) {
      if (zip !== null) {
        return zip.file('wwa.kml')!.async('string') // 4) chain with the text content promise
      }
    })
    .then(function (xml) {
      return kml(new DOMParser().parseFromString(xml as string, 'text/xml'))
    })
  return x
}

export async function fetchRoute(
  origin: [number, number],
  destination: [number, number],
  departureWindowStart: string,
  departureWindowEnd: string,
  useAIFS: string
) {
  const requestParams = {
    DepartureTimeRFC3339: addMinutes(1).toISOString(),
    StartingLocation: `${origin[1]},${origin[0]}`,
    EndingLocation: `${destination[1]},${destination[0]}`,
    DepartureWindowHoursStart: departureWindowStart,
    DepartureWindowHoursEnd: departureWindowEnd,
    AllPossibleDepartures: 'true',
    GeoJSON: 'y',
    UseAIFS: useAIFS,
  }
  const url = `https://routewx-optimize-departure-a30pyusi.uc.gateway.dev/optimize?key=${key}`
  const x = await fetch(url, {
    method: 'POST',
    body: JSON.stringify(requestParams),
  })
    .then((resp) => resp.json())
    .catch(console.error)

  return x
}
