import mapboxgl, { Map, LngLatLike, GeolocateControl } from 'mapbox-gl'

import MapboxGeocoder from '@mapbox/mapbox-gl-geocoder'
import { Position } from 'geojson'

const accessToken = process.env.NEXT_PUBLIC_MAPBOX_ACCESS_TOKEN
const style = process.env.NEXT_PUBLIC_MAPBOX_STYLE
/* Given a query in the form "lng, lat" or "lat, lng"
 * returns the matching geographic coordinate(s)
 * as search results in carmen geojson format,
 * https://github.com/mapbox/carmen/blob/master/carmen-geojson.md */
export function coordinatesGeocoder(query: string) {
  // Match anything which looks like
  // decimal degrees coordinate pair.
  const matches = query.match(
    /^[ ]*(?:Lat: )?(-?\d+\.?\d*)[, ]+(?:Lng: )?(-?\d+\.?\d*)[ ]*$/i
  )
  if (!matches) {
    return []
  }

  function coordinateFeature(lng: number, lat: number) {
    return {
      center: [lng, lat],
      geometry: {
        type: 'Point',
        coordinates: [lng, lat],
      },
      place_name: 'Lat: ' + lat + ' Lng: ' + lng,
      place_type: ['coordinate'],
      properties: {},
      type: 'Feature',
    } as MapboxGeocoder.Result
  }

  const coord1 = Number(matches[1])
  const coord2 = Number(matches[2])
  const geocodes = []

  if (coord1 < -90 || coord1 > 90) {
    // must be lng, lat
    geocodes.push(coordinateFeature(coord1, coord2))
  }

  if (coord2 < -90 || coord2 > 90) {
    // must be lat, lng
    geocodes.push(coordinateFeature(coord2, coord1))
  }

  if (geocodes.length === 0) {
    // else could be either lng, lat or lat, lng
    geocodes.push(coordinateFeature(coord1, coord2))
    geocodes.push(coordinateFeature(coord2, coord1))
  }

  return geocodes
}

export function createMap(
  container: string,
  center: LngLatLike,
  zoom: number,
  originGeocoder: MapboxGeocoder
) {
  const map = new Map({
    accessToken,
    style,
    container,
    center,
    zoom,
    maxZoom: 7,
    minZoom: 3,
  })
  const geolocate = new GeolocateControl({
    positionOptions: {
      enableHighAccuracy: true,
    },
    // When active the map will receive updates to the device's location as it changes.
    trackUserLocation: true,
    // Draw an arrow next to the location dot to indicate which direction the device is heading.
    showUserHeading: true,
  })
  // Add geolocate control to the map.
  map.addControl(geolocate)
  geolocate.on('geolocate', (data: any) => {
    console.log(data)
    if (data.coords) {
      originGeocoder.setInput(
        data.coords.latitude + ',' + data.coords.longitude
      )
    }
  })
  return map
}

export function addRasterLayer(
  map: Map,
  currentFrameUrl: string,
  currentFrameDateStr: string,
  opacity: number
) {
  map?.addSource(currentFrameDateStr, {
    type: 'raster',
    scheme: 'xyz',
    tiles: [currentFrameUrl + '/tiles/{z}/{x}/{y}.png'],
    tileSize: 256,
    attribution:
      '<a href="https://vlab.noaa.gov/web/wdtd/-/seamless-hybrid-scan-reflectivity-shsr-">Seamless Hybrid-Scan Reflectivity</a>',
  })

  map?.addLayer(
    {
      id: currentFrameDateStr,
      type: 'raster',
      source: currentFrameDateStr,
      paint: {
        'raster-opacity': opacity,
      },
    },
    'warnings-layer'
  )
}

export function addRasterLayerNoWarnings(
  map: Map,
  currentFrameUrl: string,
  currentFrameDateStr: string,
  opacity: number
) {
  map?.addSource(currentFrameDateStr, {
    type: 'raster',
    scheme: 'xyz',
    tiles: [currentFrameUrl + '/tiles/{z}/{x}/{y}.png'],
    tileSize: 256,
    attribution:
      '<a href="https://vlab.noaa.gov/web/wdtd/-/seamless-hybrid-scan-reflectivity-shsr-">Seamless Hybrid-Scan Reflectivity</a>',
  })

  map?.addLayer({
    id: currentFrameDateStr,
    type: 'raster',
    source: currentFrameDateStr,
    paint: {
      'raster-opacity': opacity,
    },
  })
}

export function showRasterLayer(
  map: Map,
  duration: number,
  opacity: number,
  frameName: string
) {
  map?.setPaintProperty(frameName, 'raster-opacity-transition', {
    duration: duration,
    delay: duration,
  })
  map?.setPaintProperty(frameName, 'raster-opacity', opacity)
}

export function hideRasterLayer(map: Map, duration: number, frameName: string) {
  map?.setPaintProperty(frameName, 'raster-opacity-transition', {
    duration: duration,
    delay: duration,
  })
  map?.setPaintProperty(frameName, 'raster-opacity', 0)
}
