module.exports = {
  extends: [
    'eslint:recommended', // ESLint's own recommended rules
    'plugin:prettier/recommended', // Prettier's ESLint rules
  ],
  plugins: ['prettier'], // Enable Prettier plugin
  rules: {
    'prettier/prettier': 'error', // Ensure Prettier errors are shown as ESLint errors
  },
}
