import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTemperatureLow } from '@fortawesome/free-solid-svg-icons'; // Import the icon

// Define the types for the props
interface TemperatureButtonProps {
  selectedProperty: string | null; // Type for the selectedProperty (could be a string or null)
  setSelectedProperty: (property: string | null) => void; // Type for the setter function (updates selectedProperty)
}

// Update the component to use TypeScript and the defined types
const TemperatureButton: React.FC<TemperatureButtonProps> = ({ selectedProperty, setSelectedProperty }) => {
  return (
    <button
      className={`inline-flex justify-center rounded-md px-4 py-2 text-sm font-medium text-white ${selectedProperty === 'temperature' ? 'bg-blue-500' : 'bg-gray-500'}`}
      onClick={() => setSelectedProperty(selectedProperty === 'temperature' ? null : 'temperature')}
    >
      <FontAwesomeIcon icon={faTemperatureLow} />
      <span>Temperature</span>
    </button>
  );
};

export default TemperatureButton;
