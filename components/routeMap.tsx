import { useRef, useEffect, useState } from 'react'
import { render } from 'react-dom'
import { fetchRoute, fetchWWA } from 'lib/backendFunctions'
import { addWatchesWarningsNew, PopupTemplate } from 'lib/alertFunctions'
import Spinner from 'components/icons/spinner'
import Pane from 'components/pane'
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Button,
} from '@material-ui/core'

import ReactDOMServer from 'react-dom/server'
import mapboxgl, { Popup } from 'mapbox-gl'
import MapboxGeocoder from '@mapbox/mapbox-gl-geocoder'
import { LngLatLike } from 'mapbox-gl'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBars } from '@fortawesome/free-solid-svg-icons'
import { mapConfig } from './homeMap'
import {
  parseFrameDate,
  getFrameUrl,
  formatDate,
  secondsToHM,
  formatHours,
} from 'lib/dateFunctions'
import {
  createMap,
  hideRasterLayer,
  showRasterLayer,
  addRasterLayerNoWarnings,
  coordinatesGeocoder,
} from 'lib/mapboxFunctions'

import 'react-sliding-pane/dist/react-sliding-pane.css'
import 'mapbox-gl/dist/mapbox-gl.css'
import '@mapbox/mapbox-gl-geocoder/dist/mapbox-gl-geocoder.css'
import Modal from 'components/modal'
import Link from 'next/link'
import { FeatureCollection, Geometry } from 'geojson'
import Guidance from './guidance'
import TemperatureButton from './temperatureButton'

const originGeocoder = new MapboxGeocoder({
  accessToken: process.env.NEXT_PUBLIC_MAPBOX_ACCESS_TOKEN as string,
  mapboxgl: mapboxgl as any,
  placeholder: 'Origin',
  localGeocoder: coordinatesGeocoder,
})

const destinationGeocoder = new MapboxGeocoder({
  accessToken: process.env.NEXT_PUBLIC_MAPBOX_ACCESS_TOKEN as string,
  mapboxgl: mapboxgl as any,
  placeholder: 'Destination',
})
const hazards = {
  green: 'No Hazards',
  orange: 'Gusty Winds or Light to Moderate Precipitation',
  red: 'Hazardous Winds or Heavy/Freezing Precip/Low Visibility',
}

function popupForSegment(segment: any, units: string) {
  const d = new Date(segment.forecast_valid_timestamp_utc + 'Z')
  const time = formatDate(d)
  const convertTemperature = (temperature_f: number, units: string) =>
    units === 'Imperial'
      ? `${temperature_f.toFixed(2)}°F`
      : `${(((temperature_f - 32) * 5) / 9).toFixed(2)}°C`

  const convertPrecip = (
    one_hour_accum_precip_inches: number,
    units: string,
  ) =>
    units === 'Imperial'
      ? `${one_hour_accum_precip_inches.toFixed(2)} inches`
      : `${(one_hour_accum_precip_inches * 2.54).toFixed(2)} cm`

  const convertWind = (wind_gust_mph: number, units: string) =>
    units === 'Imperial'
      ? `${wind_gust_mph.toFixed(2)} mph`
      : `${(wind_gust_mph * 1.60934).toFixed(2)} km/hr`
  const convertVis = (visibility_miles: number, units: string) =>
    units === 'Imperial'
      ? `${visibility_miles.toFixed(2)} miles`
      : `${(visibility_miles * 1.60934).toFixed(2)} km`

  return ReactDOMServer.renderToStaticMarkup(
    <div>
      <h2
        style={{
          borderRadius: '10px',
          textAlign: 'center',
          margin: '5px',
          color: 'white',
          backgroundColor: segment.stroke,
        }}
        className='font-lg'
      >
        {hazards[segment.hazard_level as keyof typeof hazards]}
      </h2>
      <div>
        Forecast for 🕰️ : <p style={{ fontWeight: 600 }}>{time}</p>
      </div>
      <div>
        <p style={{ display: 'inline-block' }}>Temperature 🌡️ : </p>
        <p style={{ display: 'inline-block', fontWeight: 100 }}>
          &nbsp;{convertTemperature(segment.temperature_f, units)}
        </p>
      </div>
      <div>
        <p style={{ display: 'inline-block' }}>Precipitation ☔️ : </p>
        <p style={{ display: 'inline-block', fontWeight: 100 }}>
          &nbsp;{convertPrecip(segment.one_hour_accum_precip_inches, units)}
        </p>
      </div>
      <div>
        <p style={{ display: 'inline-block' }}>Frozen Precipitation 🌨️ : </p>
        <p style={{ display: 'inline-block', fontWeight: 100 }}>
          &nbsp;{segment.probability_of_frozen_precip}%
        </p>
      </div>
      <div>
        <p style={{ display: 'inline-block' }}>Wind Gusts 🌬️ : </p>
        <p style={{ display: 'inline-block', fontWeight: 100 }}>
          &nbsp;{convertWind(segment.wind_gust_mph, units)}
        </p>
      </div>
      <div>
        <p style={{ display: 'inline-block' }}>Visibility 🌤 : </p>{' '}
        <p style={{ display: 'inline-block', fontWeight: 100 }}>
          &nbsp;{convertVis(segment.visibility_miles, units)}
        </p>
      </div>
    </div>,
  )
}

function removeOldRoute(map: mapboxgl.Map, id = 'curRoute') {
  const curLayer = map?.getLayer(id)

  if (curLayer) {
    map.removeLayer(id)
    map.removeSource(id)
  }
}

export interface RouteOption {
  DepartureTime: string
  EstimatedTravelTime: string
  HazardsFraction: string
  IsAI: boolean
  Polylines: {
    features: {
      properties: {
        temperature_f: number
        one_hour_accum_precip_inches: number
        probability_of_frozen_precip: number
        wind_gust_mph: number
        visibility_miles: number
        hazard_level: number
        forecast_valid_timestamp_utc: string
        stroke: string
        'stroke-width': number
      }
    }[]
  }
}

export interface DirectionGeoJSON {
  message: string
  polylines: FeatureCollection
  bounds: Bounds
  duration_in_traffic: number
  DepartureTime: Date
  HazardsFraction: number
}

export interface Bounds {
  Northeast: LatLng
  Southwest: LatLng
}

export interface LatLng {
  Lat: number
  Lng: number
}

export interface Properties {
  hazard_level: string
  temperature_f: number
  one_hour_accum_precip_inches: number
  probability_of_frozen_precip: number
  forecast_valid_timestamp_utc: string
  wind_gust_mph: number
  visibility_miles: number
  stroke: string
  'stroke-width': number
}

function routeOptionsDialog(result: any, map: mapboxgl.Map, isAI: boolean) {
  let routeOptions: RouteOption[] = []

  result.forEach((route: any) => {
    const hazardsMultiplier =
      route.HazardsFraction > 0 ? route.HazardsFraction * 0.2 : 1.0
    const travelTime = secondsToHM(
      route.duration_in_traffic +
        route.HazardsFraction * 0.2 * route.duration_in_traffic,
    )
    const addedTravelTime = secondsToHM(
      route.HazardsFraction * 0.2 * route.duration_in_traffic,
    )
    const weatherAdds =
      route.HazardsFraction * 0.2 * route.duration_in_traffic > 0
        ? '\n weather adds: ' + addedTravelTime
        : ''

    const routeOption: RouteOption = {
      HazardsFraction: Math.round(route.HazardsFraction * 100) + '%',
      EstimatedTravelTime: travelTime + weatherAdds,
      DepartureTime: formatDate(new Date(route.DepartureTime)),
      Polylines: route.polylines,
      IsAI: isAI,
    }
    routeOptions.push(routeOption)
  })
  return routeOptions
}

export default function RouteMap({
  className = '',
  children,
  containerId,
  dateStrings = [],
}: {
  className: string
  children?: JSX.Element | string
  containerId: string
  dateStrings: any[]
}) {
  const [open, setOpen] = useState(false)
  const [routeOptionsOpen, setRouteOptionsOpen] = useState(false)
  const [dialogueText, setDialogueText] = useState('')
  const [headlineText, setHeadlineText] = useState('')
  const [headlineColor, setHeadlineColor] = useState('green')
  const [routeOptions, setRouteOptions] = useState<RouteOption[]>(
    [] as RouteOption[],
  )
  const [routeOptionsAI, setRouteOptionsAI] = useState<RouteOption[]>(
    [] as RouteOption[],
  )
  const [openDialog, setOpenDialog] = useState(false)
  const [useAIWeather, setUseAIWeather] = useState(false)
  const [routeSelected, setRouteSelected] = useState(-1)
  const [optimalDeparture, setOptimalDeparture] = useState('')
  const [units, setUnits] = useState('Imperial')
  const [departureWindowStart, setDepartureWindowStart] = useState('0')
  const [departureWindowEnd, setDepartureWindowEnd] = useState('3')
  const [selectedOptionIsAI, setSelectedOptionIsAI] = useState(false)

  const [map, setMap] = useState<mapboxgl.Map | null>()
  const [origin, setOrigin] = useState<[number, number] | null>(null)
  const [destination, setDestination] = useState<[number, number] | null>(null)
  const [result, setResult] = useState([] as DirectionGeoJSON[])
  const [resultAI, setResultAI] = useState([] as DirectionGeoJSON[])
  const [loading, setLoading] = useState(false)
  const [mapFrames, setMapFrames] = useState([] as string[])
  const [mapLoaded, setMapLoaded] = useState(false)

  const [selectedDate, setSelectedDate] = useState(
    parseFrameDate(dateStrings[0]),
  )
  const minDate = parseFrameDate(dateStrings[0]).toTimeString()
  const maxDate = parseFrameDate(
    dateStrings[dateStrings.length - 1],
  ).toTimeString()

  const [animated, setAnimated] = useState(false)
  const [hidden, setHidden] = useState(true)
  const [isPaneOpenLeft, setPaneOpenLeft] = useState(false)

  const handleDepartureWindowStartChange = (event: any) => {
    setDepartureWindowStart(event.value)
  }
  const handleDepartureWindowEndChange = (event: any) => {
    setDepartureWindowEnd(event.value)
  }

  const handleAnimation = () => {
    setAnimated(!animated)
  }
  const handleRadar = (radarDateHidden: boolean) => {
    setHidden(radarDateHidden)
  }
  const handlePane = () => {
    setPaneOpenLeft(false)
  }

  const handleDialogClose = (confirm: boolean) => {
    setOpenDialog(false)
    if (confirm) {
      setUseAIWeather(true)
      fetchRouteWithAIWeather()
    } else {
      setUseAIWeather(false)
      fetchRouteWithoutAIWeather()
    }
  }

  const fetchRouteWithAIWeather = async () => {
    setLoading(true)
    const resultAI = await fetchRoute(
      origin as [number, number],
      destination as [number, number],
      departureWindowStart,
      departureWindowEnd,
      'y',
    )
    const result = await fetchRoute(
      origin as [number, number],
      destination as [number, number],
      departureWindowStart,
      departureWindowEnd,
      'n',
    )
    removeOldRoute(map as mapboxgl.Map)

    const roptions = routeOptionsDialog(result, map as mapboxgl.Map, false)
    const roptionsAI = routeOptionsDialog(resultAI, map as mapboxgl.Map, true)
    console.log(resultAI)
    setResult(result)
    setRouteOptions(roptions)
    setRouteOptionsOpen(true)
    setResultAI(resultAI)
    setRouteOptionsAI(roptionsAI)

    setLoading(false)
  }

  const fetchRouteWithoutAIWeather = async () => {
    setLoading(true)
    const result = await fetchRoute(
      origin as [number, number],
      destination as [number, number],
      departureWindowStart,
      departureWindowEnd,
      'n',
    )
    setLoading(false)
    removeOldRoute(map as mapboxgl.Map)

    const roptions = routeOptionsDialog(result, map as mapboxgl.Map, false)
    setResult(result)
    setRouteOptions(roptions)
    setRouteOptionsOpen(true)
  }
  const intervalIdRef = useRef<NodeJS.Timer>()
  const popUpRef = useRef(new Popup({ offset: 15 }))
  useEffect(() => {
    removeOldRoute(map as mapboxgl.Map)
    if (result !== null && routeSelected >= 0) {
      const thisRoute = selectedOptionIsAI
        ? resultAI[routeSelected]
        : result[routeSelected]
      map?.addSource('curRoute', {
        type: 'geojson',
        data: thisRoute?.polylines,
      })

      map?.addLayer({
        id: 'curRoute',
        type: 'line',
        source: 'curRoute',
        layout: {
          'line-join': 'round',
          'line-cap': 'round',
        },
        paint: {
          'line-color': ['get', 'stroke'],
          'line-width': 8,
        },
      })

      map?.on('click', 'curRoute', (e) => {
        new mapboxgl.Popup()
          .setLngLat(e.lngLat)
          .setHTML(
            popupForSegment(
              e.features ? (e.features[0].properties as any) : '',
              units,
            ),
          )
          .addTo(map)
      })
      const mapBoundAdjustmentFactor = 0.05
      map?.fitBounds([
        [
          thisRoute?.bounds.Southwest.Lng +
            thisRoute?.bounds.Southwest.Lng * mapBoundAdjustmentFactor,
          thisRoute?.bounds.Southwest.Lat -
            thisRoute?.bounds.Southwest.Lat * mapBoundAdjustmentFactor,
        ],
        [
          thisRoute?.bounds.Northeast.Lng -
            thisRoute?.bounds.Northeast.Lng * mapBoundAdjustmentFactor,
          thisRoute?.bounds.Northeast.Lat +
            thisRoute?.bounds.Northeast.Lat * mapBoundAdjustmentFactor,
        ],
      ])
      const hazardLevels = new Set()
      thisRoute?.polylines.features.forEach((feature: any) => {
        hazardLevels.add(feature.properties.hazard_level)
      })

      const travelTime = secondsToHM(thisRoute?.duration_in_traffic)
      const addedTravelTime = secondsToHM(
        thisRoute?.HazardsFraction * 0.2 * thisRoute?.duration_in_traffic,
      )
      const estimatedTravelTime = 'Estimated Travel Time: ' + travelTime + '.'
      const hazardsFraction =
        ' with ' +
        Math.round(thisRoute?.HazardsFraction * 100) +
        ' percent of the route impacted by weather, which could add ' +
        addedTravelTime +
        ' to your travel time.'
      setOptimalDeparture(formatDate(new Date(thisRoute?.DepartureTime)))
      if (hazardLevels.has('red')) {
        setHeadlineColor('red')
        setHeadlineText('Heavy and/or Freezing Precipitation/High Winds.')
        setDialogueText(
          estimatedTravelTime +
            hazardsFraction +
            '\n Tap Route for forecast details.',
        )
      } else if (hazardLevels.has('orange')) {
        setHeadlineColor('orange')
        setHeadlineText('Moderate Hazard Wind/Precipitation Possible.')
        setDialogueText(
          estimatedTravelTime +
            hazardsFraction +
            '\n Tap Route for forecast details.',
        )
      } else {
        setHeadlineText('No Weather Hazards.')
        setDialogueText(
          estimatedTravelTime + '\n Tap Route for forecast details.',
        )
      }
      setOpen(true)
    }

    // Perform additional actions or function calls
    // ...
  }, [routeSelected])
  useEffect(() => {
    const queryParams = new URLSearchParams(window.location.search)
    const centerLat = queryParams.get('centerLat') ?? '40'
    const centerLon = queryParams.get('centerLon') ?? '-100'
    const zoom = queryParams.get('zoom') ?? '4'
    const centerLatInt = parseInt(centerLat, 10)
    const centerLonInt = parseInt(centerLon, 10)
    const zoomInt = parseInt(zoom, 10)

    const map = createMap(
      containerId,
      [centerLonInt, centerLatInt] as LngLatLike,
      zoomInt,
      originGeocoder,
    )

    originGeocoder.on('result', ({ result }) => {
      if (result.type == 'Feature') {
        if (result.place_name.includes('United States')) {
          setUnits('Imperial')
        } else {
          setUnits('Metric')
        }
      }
      setOrigin(result.center)
      removeOldRoute(map)
    })

    originGeocoder.on('clear', () => {
      setOrigin(null)
      removeOldRoute(map)
    })

    destinationGeocoder.on('result', ({ result }) => {
      if (result.type == 'Feature') {
        if (result.place_name.includes('United States')) {
          setUnits('Imperial')
        } else {
          setUnits('Metric')
        }
      }
      setDestination(result.center)
      removeOldRoute(map)
    })

    destinationGeocoder.on('clear', () => {
      setDestination(null)
      removeOldRoute(map)
    })

    document
      .getElementById('originGeocoder')
      ?.appendChild(originGeocoder.onAdd(map))
    document
      .getElementById('destinationGeocoder')
      ?.appendChild(destinationGeocoder.onAdd(map))

    map.on('load', () => {
      setMapLoaded(true)
    })

    setMap(map)
    setMapFrames(dateStrings)

    return () => {
      const origGeoParent = document.getElementById(
        'originGeocoder',
      ) as HTMLElement
      if (origGeoParent && origGeoParent.firstChild) {
        origGeoParent.removeChild(origGeoParent.firstChild)
        originGeocoder.onRemove()
      }

      const destGeoParent = document.getElementById(
        'destinationGeocoder',
      ) as HTMLElement
      if (destGeoParent && destGeoParent.firstChild) {
        destGeoParent.removeChild(destGeoParent.firstChild)
        destinationGeocoder.onRemove()
      }
    }
  }, [containerId])
  async function getWWA(map: mapboxgl.Map) {
    const wwaFeatureCollection = await fetchWWA()
    if (wwaFeatureCollection !== null) {
      addWatchesWarningsNew(map, wwaFeatureCollection as FeatureCollection)
      map.on('click', 'watches-warnings-layer', (e) => {
        const coordinates = e?.lngLat
        const Lng = coordinates.lng
        const Lat = coordinates.lat
        const url = `https://api.weather.gov/alerts/active?point=${Lat},${Lng}`
        if (coordinates) {
          const popupNode = document.createElement('div')
          fetch(url)
            .then((response) => response.json())
            .then((data) => {
              if (data.features.length > 0) {
                const firstFeature = data.features[0]
                render(
                  <PopupTemplate
                    headline={firstFeature.properties.headline}
                    description={firstFeature.properties.description}
                    product={firstFeature.properties.event}
                  />,
                  popupNode,
                )
                popUpRef.current
                  .setLngLat(coordinates)
                  .setDOMContent(popupNode)
                  .addTo(map)
              }
            })
        }
      })
    }
  }

  useEffect(() => {
    if (map && mapLoaded) {
      let wwaLayer = map.getLayer('watches-warnings-layer')
      if (typeof wwaLayer === 'undefined') {
        getWWA(map)
      }
      for (var frame of mapFrames) {
        if (map.getLayer(frame)) {
          map.removeLayer(frame)
        }
        if (map.getSource(frame)) {
          map.removeSource(frame)
        }
      }

      if (intervalIdRef.current) {
        clearInterval(intervalIdRef.current)
      }

      if (animated) {
        intervalIdRef.current = setInterval(() => {
          if (dateStrings.length > 0) {
            const previousFrameDateStr = dateStrings[mapConfig.currentFrame]
            let oldLayer = map.getLayer(previousFrameDateStr)

            mapConfig.currentFrame =
              (mapConfig.currentFrame + 1) % dateStrings.length

            let currentFrameDateStr = dateStrings[mapConfig.currentFrame]
            let currentFrameUrl = getFrameUrl(currentFrameDateStr)
            let currentFrameDate = parseFrameDate(currentFrameDateStr)
            let shsrLayer = map.getLayer(currentFrameDateStr)

            setSelectedDate(currentFrameDate)

            if (mapConfig.shsrVisible) {
              if (typeof shsrLayer === 'undefined') {
                addRasterLayerNoWarnings(
                  map,
                  currentFrameUrl,
                  currentFrameDateStr,
                  mapConfig.shsrOpacity,
                )
              } else {
                showRasterLayer(
                  map,
                  mapConfig.animationDuration,
                  mapConfig.shsrOpacity,
                  currentFrameDateStr,
                )
              }
            }
            if (oldLayer) {
              hideRasterLayer(
                map,
                mapConfig.animationDuration,
                previousFrameDateStr,
              )
            }
          }
        }, 250)
      } else {
        if (dateStrings.length > 0) {
          let currentFrameDateStr = dateStrings[dateStrings.length - 1]
          let currentFrameUrl = getFrameUrl(currentFrameDateStr)
          let currentFrameDate = parseFrameDate(currentFrameDateStr)
          let shsrLayer = map.getLayer(currentFrameDateStr)

          setSelectedDate(currentFrameDate)

          if (mapConfig.shsrVisible) {
            if (typeof shsrLayer === 'undefined') {
              addRasterLayerNoWarnings(
                map,
                currentFrameUrl,
                currentFrameDateStr,
                mapConfig.shsrOpacity,
              )
            } else {
              showRasterLayer(
                map,
                mapConfig.animationDuration,
                mapConfig.shsrOpacity,
                currentFrameDateStr,
              )
            }
          }
        }
      }
    }
  }, [map, mapLoaded, animated])

  const getRoute = async () => {
    setOpenDialog(true)
  }

  return (
    <div id={containerId} className={`${className} reLative`}>
      {children}
      <div className='absolute left-4 z-[100] mt-2'>
        <Link href='/' title='home' className='cursor-pointer'>
          <img
            alt='RouteWx logo'
            src='/favicon.png'
            className='h-12 w-12 rounded-full shadow-md duration-200 hover:rotate-45'
          />
        </Link>
        <div className='mt-2 flex items-center justify-center'>
          <FontAwesomeIcon
            size='3x'
            icon={faBars}
            onClick={() => setPaneOpenLeft(true)}
          />
        </div>
      </div>
      <div className='absolute top-4 right-4 z-40 flex flex-col space-y-4 md:flex-row md:space-y-0 md:space-x-4'>
        <span className='z-40' id='originGeocoder' />
        <span className='z-30' id='destinationGeocoder' />
        <button
          className='cursor-pointer rounded-md border border-transparent bg-indigo-100 px-6 py-1 text-base font-medium text-indigo-700 shadow-md hover:bg-indigo-200 focus:outline-none disabled:cursor-not-allowed disabled:bg-indigo-100 disabled:text-gray-400'
          disabled={origin === null || destination === null}
          onClick={getRoute}
        >
          Get Route
        </button>
        <button
          className='cursor-pointer rounded-md border border-transparent bg-indigo-100 px-6 py-1 text-base font-medium text-indigo-700 shadow-md hover:bg-indigo-200 focus:outline-none disabled:cursor-not-allowed disabled:bg-indigo-100 disabled:text-gray-400'
          disabled={routeSelected == -1}
          onClick={() => setRouteOptionsOpen(true)}
        >
          Show Departure Options
        </button>
      </div>
      {loading && (
        <div className='absolute inset-0 z-50 flex cursor-not-allowed items-center justify-center'>
          <div className='absolute inset-0 cursor-not-allowed bg-gray-500 opacity-50' />
          <Spinner />
        </div>
      )}
      <Guidance
        open={routeOptionsOpen}
        routeOptions={routeOptions}
        routeOptionsAI={routeOptionsAI}
        setRouteOptionsOpen={setRouteOptionsOpen}
        setRouteSelected={setRouteSelected}
        setSelectedOptionIsAI={setSelectedOptionIsAI}
      />
      <Dialog open={openDialog} onClose={() => handleDialogClose(false)}>
        <DialogTitle>Use Experimental AI Weather Forecasts?</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Do you want to use experimental AI generated weather forecasts?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => handleDialogClose(false)} color='primary'>
            No
          </Button>
          <Button
            onClick={() => handleDialogClose(true)}
            color='primary'
            autoFocus
          >
            Yes
          </Button>
        </DialogActions>
      </Dialog>
      <Modal
        open={open}
        color={headlineColor}
        headline={headlineText}
        optimalDeparture={optimalDeparture}
        departureWindow={
          formatHours(departureWindowStart) +
          ' & ' +
          formatHours(departureWindowEnd)
        }
        setOpen={setOpen}
        title='RouteWx Travel Guidance'
        text={dialogueText}
      />
      <Pane
        animationCallback={handleAnimation}
        radarDateCallback={handleRadar}
        departureWindowStartCallback={handleDepartureWindowStartChange}
        departureWindowEndCallback={handleDepartureWindowEndChange}
        departureWindowStart={departureWindowStart}
        departureWindowEnd={departureWindowEnd}
        paneCallback={handlePane}
        isPaneOpenLeft={isPaneOpenLeft}
      />
      {!hidden ? (
        <div className='show-datetime absolute bottom-0.5 pb-1'>
          {animated && (
            <div>
              <div>Oldest Time: {minDate}</div>
              <div>Latest Time: {maxDate}</div>
            </div>
          )}
          <p>Current Radar Time: {selectedDate.toString()}</p>
        </div>
      ) : null}
    </div>
  )
}
