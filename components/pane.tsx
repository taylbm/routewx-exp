import SlidingPane from 'react-sliding-pane'
import Select from 'react-select'
import { useState } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSquareCheck, faSquare } from '@fortawesome/free-regular-svg-icons'
import { faCircleXmark } from '@fortawesome/free-regular-svg-icons'

import { hourOptions } from 'lib/dateFunctions'

const startOptions = hourOptions(0, 13)
const endOptions = hourOptions(1, 25)

export default function Pane(props: {
  animationCallback: any
  radarDateCallback: any
  departureWindowStartCallback: any
  departureWindowEndCallback: any
  departureWindowStart: string
  departureWindowEnd: string
  paneCallback: any
  isPaneOpenLeft: boolean
}) {
  const [animated, setAnimated] = useState(false)
  const [hidden, setHidden] = useState(true)
  const [isPaneOpenLeft, setPaneOpenLeft] = useState(false)

  const handleRadarDateClick = () => {
    setHidden(!hidden)
    props.radarDateCallback(!hidden)
  }
  const handleAnimationClick = () => {
    setAnimated(!animated)
    props.animationCallback()
  }

  return (
    <SlidingPane
      closeIcon={
        <div>
          <FontAwesomeIcon
            style={{ width: 'auto' }}
            icon={faCircleXmark}
            size='2x'
            className='rounded-lg bg-blue-600 text-white'
          />
        </div>
      }
      isOpen={props.isPaneOpenLeft}
      title='Settings'
      from='left'
      overlayClassName='left-pane-overlay'
      onRequestClose={props.paneCallback}
    >
      <div className='flex flex-col bg-gray-200'>
        <div className='m-2 rounded-lg bg-blue-600 px-4 py-2 text-center text-white'>
          <button>
            <FontAwesomeIcon
              size='2x'
              icon={animated ? faSquareCheck : faSquare}
              onClick={handleAnimationClick}
            ></FontAwesomeIcon>
            {`${animated ? ' Stop Animating' : ' Animate'} Radar`}
          </button>
        </div>
        <div className='m-2 rounded-lg bg-blue-600 px-4 py-2 text-center text-white'>
          <button className={'hide-button'} onClick={handleRadarDateClick}>
            <FontAwesomeIcon
              size='2x'
              icon={hidden ? faSquare : faSquareCheck}
            ></FontAwesomeIcon>
            {`${hidden ? ' Show' : ' Hide'} Radar Time`}
          </button>
        </div>
        <Select
          options={startOptions}
          onChange={props.departureWindowStartCallback}
          className='right-0 z-50'
          placeholder='Departure Window Start (default is current time)'
        />
        <br></br>
        <Select
          options={endOptions}
          onChange={props.departureWindowEndCallback}
          className='right-0 z-40'
          placeholder='Departure Window End (default is +3 hours)'
        />
      </div>
    </SlidingPane>
  )
}
