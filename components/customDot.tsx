import React from 'react'

interface CustomizedDotProps {
  cx: number
  cy: number
  stroke: string
  payload: any
  value: number
}

const CustomizedDot: React.FC<CustomizedDotProps> = ({
  cx,
  cy,
  stroke,
  payload,
  value,
}) => {
  if (isNaN(cx) || isNaN(cy)) return null // Handle edge cases where cx or cy is invalid

  // Function to create a star path
  const createStarPath = (
    cx: number,
    cy: number,
    spikes: number,
    outerRadius: number,
    innerRadius: number,
  ) => {
    const step = Math.PI / spikes
    let path = ''
    for (let i = 0; i < 2 * spikes; i++) {
      const radius = i % 2 === 0 ? outerRadius : innerRadius
      const x = cx + Math.cos(i * step) * radius
      const y = cy + Math.sin(i * step) * radius // Ensure y increases downwards in SVG
      path += `${i === 0 ? 'M' : 'L'} ${x} ${y} `
    }
    return `${path}Z`
  }

  const starPath = createStarPath(cx, cy, 5, 8, 4) // 5-point star

  return (
    <svg>
      {value > 0 && ( // Only render the star if value is greater than 0
        <path d={starPath} stroke={stroke} strokeWidth={2} fill='white' />
      )}
      {value > 50 && (
        <text
          x={cx}
          y={cy + 10} // Adjust y position for text to avoid overlap with the star
          textAnchor='middle'
          dominantBaseline='middle'
          fill={stroke}
        >
          {value}
        </text>
      )}
    </svg>
  )
}

export default CustomizedDot
