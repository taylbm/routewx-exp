import { Fragment, useState, useMemo } from 'react'
import { Dialog, Transition } from '@headlessui/react'
import { RouteOption } from 'components/routeMap'
import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
} from 'recharts'
import { parseISO } from 'date-fns'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
  faCheckCircle,
  faCircleXmark,
} from '@fortawesome/free-regular-svg-icons'
import {
  faTemperatureLow,
  faCloud,
  faChartLine,
  faWind,
  faUmbrella,
} from '@fortawesome/free-solid-svg-icons'
import CustomDot from './customDot'

// ...existing code...
function fToC(f: number) {
  return Number(((f - 32) * (5 / 9)).toFixed(2))
}
function milesToKm(m: number) {
  return Number((m * 1.60934).toFixed(2))
}
function mphToKph(m: number) {
  return Number((m * 1.60934).toFixed(2))
}
function inchesToMm(i: number) {
  return Number((i * 25.4).toFixed(2))
}

const varMapImperial = {
  temperature_f: '(°F)',
  visibility_miles: '(Miles)',
  wind_gust_mph: '(mph)',
  one_hour_accum_precip_inches: '(inches)',
}

const varMapMetric = {
  temperature_f: '(°C)',
  visibility_miles: '(km)',
  wind_gust_mph: '(kph)',
  one_hour_accum_precip_inches: '(mm)',
}

const colors = [
  '#E69F00',
  '#56B4E9',
  '#009E73',
  '#F0E442',
  '#0072B2',
  '#D55E00',
  '#CC79A7',
  '#999999',
]

const getRandomColorByIndex = (index: number): string =>
  colors[index % colors.length]

export default function Guidance({
  open,
  routeOptions,
  routeOptionsAI,
  setRouteOptionsOpen,
  setRouteSelected,
  setSelectedOptionIsAI,
}: {
  open: boolean
  routeOptions: RouteOption[] | null | undefined
  routeOptionsAI?: RouteOption[] | null | undefined
  setRouteOptionsOpen: React.Dispatch<React.SetStateAction<boolean>>
  setRouteSelected: React.Dispatch<React.SetStateAction<number>>
  setSelectedOptionIsAI: React.Dispatch<React.SetStateAction<boolean>>
}) {
  const [showGraph, setShowGraph] = useState(false)
  const [selectedProperty, setSelectedProperty] = useState<
    | 'temperature_f'
    | 'visibility_miles'
    | 'wind_gust_mph'
    | 'one_hour_accum_precip_inches'
  >('temperature_f')
  const [unit, setUnit] = useState<'imperial' | 'metric'>('imperial')

  const graphData = useMemo(() => {
    const allOptions = [...(routeOptions || []), ...(routeOptionsAI || [])]
    const forecastTimesSet = new Set<number>()

    allOptions.forEach((option) => {
      option.Polylines?.features.forEach((segment) => {
        const timestamp = parseISO(
          segment.properties.forecast_valid_timestamp_utc,
        ).getTime()
        forecastTimesSet.add(timestamp)
      })
    })
    const forecastTimes = Array.from(forecastTimesSet).sort((a, b) => a - b)

    const departureTimes = allOptions.map((option) => ({
      departureTime: option.DepartureTime,
      isAI: option.IsAI,
    }))

    const data: any[] = forecastTimes.map((time) => ({
      forecast_valid_timestamp_utc: time,
    }))

    allOptions.forEach((option) => {
      option.Polylines?.features.forEach((segment) => {
        const time = parseISO(
          segment.properties.forecast_valid_timestamp_utc,
        ).getTime()
        const dataKey = `value_${option.DepartureTime}${
          option.IsAI ? '_AI' : ''
        }`
        const dataPoint = data.find(
          (d) => d.forecast_valid_timestamp_utc === time,
        )
        if (dataPoint) {
          let rawValue = segment.properties.temperature_f
          switch (selectedProperty) {
            case 'temperature_f':
              rawValue = segment.properties.temperature_f
              break
            case 'visibility_miles':
              rawValue = segment.properties.visibility_miles
              break
            case 'wind_gust_mph':
              rawValue = segment.properties.wind_gust_mph
              break
            case 'one_hour_accum_precip_inches':
              rawValue = segment.properties.one_hour_accum_precip_inches
              break
          }
          if (unit === 'metric') {
            switch (selectedProperty) {
              case 'temperature_f':
                dataPoint[dataKey] = fToC(rawValue)
                break
              case 'visibility_miles':
                dataPoint[dataKey] = milesToKm(rawValue)
                break
              case 'wind_gust_mph':
                dataPoint[dataKey] = mphToKph(rawValue)
                break
              case 'one_hour_accum_precip_inches':
                dataPoint[dataKey] = inchesToMm(rawValue)
                break
            }
          } else {
            dataPoint[dataKey] = rawValue
          }
        }
      })
    })

    return { data, departureTimes }
  }, [routeOptions, routeOptionsAI, selectedProperty, unit])

  const departureTimes = useMemo(() => {
    return graphData.departureTimes
  }, [graphData])

  const getDataKey = (departureTime: string, isAI: boolean) => {
    return `value_${departureTime}${isAI ? '_AI' : ''}`
  }

  const getName = (departureTime: string, isAI: boolean) => {
    return `Departure ${departureTime}${isAI ? ' (AI)' : ''} ${
      unit == 'imperial'
        ? varMapImperial[selectedProperty]
        : varMapMetric[selectedProperty]
    }`
  }

  const formatTime = (timestamp: number) => {
    const d = new Date()
    const adjustedTimestamp = timestamp - d.getTimezoneOffset() * 60 * 1000
    return new Date(adjustedTimestamp).toLocaleTimeString([], {
      hour: '2-digit',
      minute: '2-digit',
      hour12: true,
    })
  }

  return (
    <Transition.Root show={open} as={Fragment}>
      <Dialog
        as='div'
        className='fixed inset-0 z-10 overflow-y-auto'
        onClose={setRouteOptionsOpen}
      >
        <div className='flex min-h-screen items-end justify-center pt-4 pb-20 text-center sm:block sm:p-0'>
          <Transition.Child
            as={Fragment}
            enter='ease-out duration-300'
            enterFrom='opacity-0'
            enterTo='opacity-100'
            leave='ease-in duration-200'
            leaveFrom='opacity-100'
            leaveTo='opacity-0'
          >
            <Dialog.Overlay className='fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity' />
          </Transition.Child>
          <span
            className='hidden sm:inline-block sm:h-screen sm:align-middle'
            aria-hidden='true'
          >
            &#8203;
          </span>
          <Transition.Child
            as={Fragment}
            enter='ease-out duration-300'
            enterFrom='opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95'
            enterTo='opacity-100 translate-y-0 sm:scale-100'
            leave='ease-in duration-200'
            leaveFrom='opacity-100 translate-y-0 sm:scale-100'
            leaveTo='opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95'
          >
            <div className='inline-block w-full transform rounded-lg bg-white pt-5 pb-4 text-left align-bottom shadow-xl transition-all'>
              <div>
                <div className='mt-2'>
                  <h2 className='text-center'>
                    <b>Route Options:</b>
                  </h2>
                  <div className='mb-2 flex flex-wrap items-center justify-between px-4'>
                    <div className='flex'>
                      <button
                        className={`inline-flex justify-center rounded-md px-4 py-2 text-sm font-medium text-white ${
                          selectedProperty === 'temperature_f'
                            ? 'bg-red-700'
                            : 'bg-red-600 hover:bg-red-700'
                        } mr-2`}
                        onClick={() => setSelectedProperty('temperature_f')}
                      >
                        <FontAwesomeIcon icon={faTemperatureLow} />
                        <span className='ml-1'>Temperature</span>
                      </button>
                      <button
                        type='button'
                        className={`inline-flex justify-center rounded-md px-4 py-2 text-sm font-medium text-white ${
                          selectedProperty === 'visibility_miles'
                            ? 'bg-gray-700'
                            : 'bg-gray-600 hover:bg-gray-700'
                        } mr-2`}
                        onClick={() => setSelectedProperty('visibility_miles')}
                      >
                        <FontAwesomeIcon icon={faCloud} />
                        <span className='ml-1'>Visibility</span>
                      </button>
                      <button
                        type='button'
                        className={`inline-flex justify-center rounded-md px-4 py-2 text-sm font-medium text-white ${
                          selectedProperty === 'wind_gust_mph'
                            ? 'bg-blue-700'
                            : 'bg-blue-600 hover:bg-blue-700'
                        } mr-2`}
                        onClick={() => setSelectedProperty('wind_gust_mph')}
                      >
                        <FontAwesomeIcon icon={faWind} />
                        <span className='ml-1'>Wind</span>
                      </button>
                      <button
                        type='button'
                        className={`inline-flex justify-center rounded-md px-4 py-2 text-sm font-medium text-white ${
                          selectedProperty === 'one_hour_accum_precip_inches'
                            ? 'bg-purple-700'
                            : 'bg-purple-600 hover:bg-purple-700'
                        } mr-2`}
                        onClick={() =>
                          setSelectedProperty('one_hour_accum_precip_inches')
                        }
                      >
                        <FontAwesomeIcon icon={faUmbrella} />
                        <span className='ml-1'>Precip</span>
                      </button>
                    </div>
                    <div className='flex'>
                      <button
                        type='button'
                        className='mr-2 inline-flex justify-center rounded-md bg-blue-600 px-4 py-2 text-sm font-medium text-white hover:bg-blue-700'
                        onClick={() => setShowGraph(!showGraph)}
                      >
                        <FontAwesomeIcon icon={faChartLine} />
                        <span className='ml-1'>
                          {showGraph ? 'Hide Graph' : 'Show Graph'}
                        </span>
                      </button>
                      <button
                        type='button'
                        className={`inline-flex justify-center rounded-md px-2 py-2 text-sm font-medium text-white ${
                          unit === 'imperial'
                            ? 'bg-green-700'
                            : 'bg-green-600 hover:bg-green-700'
                        } mr-2`}
                        onClick={() => setUnit('imperial')}
                      >
                        Imperial
                      </button>
                      <button
                        type='button'
                        className={`inline-flex justify-center rounded-md px-2 py-2 text-sm font-medium text-white ${
                          unit === 'metric'
                            ? 'bg-green-700'
                            : 'bg-green-600 hover:bg-green-700'
                        }`}
                        onClick={() => setUnit('metric')}
                      >
                        Metric
                      </button>
                    </div>
                  </div>
                  {showGraph && (
                    <div className='mb-4 h-80 w-full px-4'>
                      <ResponsiveContainer width='100%' height='100%'>
                        <LineChart data={graphData.data}>
                          <CartesianGrid strokeDasharray='3 3' />
                          <XAxis
                            dataKey='forecast_valid_timestamp_utc'
                            tickFormatter={formatTime}
                            type='number'
                            domain={['auto', 'auto']}
                            scale='time'
                            minTickGap={20}
                            tick={{ fontSize: 12 }}
                          />
                          <YAxis />
                          <Tooltip
                            labelFormatter={(label) => formatTime(label)}
                          />
                          <Legend />
                          {departureTimes.map(
                            ({ departureTime, isAI }, index) => (
                              <Line
                                key={getDataKey(departureTime, isAI)}
                                type='monotone'
                                dataKey={getDataKey(departureTime, isAI)}
                                name={getName(departureTime, isAI)}
                                stroke={getRandomColorByIndex(index)}
                                activeDot={{
                                  r: isAI ? 8 : 4,
                                  stroke: isAI ? 'green' : 'blue',
                                  fill: getRandomColorByIndex(index),
                                }}
                                legendType={isAI ? 'star' : 'circle'}
                                dot={
                                  isAI
                                    ? (props) => <CustomDot {...props} />
                                    : undefined
                                }
                              />
                            ),
                          )}
                        </LineChart>
                      </ResponsiveContainer>
                    </div>
                  )}
                  <div className='overflow-x-auto px-4'>
                    <table className='w-full text-xs'>
                      <thead>
                        <tr>
                          <th className='py-2'>Departure Time</th>
                          <th className='py-2'>Estimated Travel Time</th>
                          <th className='py-2'>
                            Fraction of Route Impacted by Weather
                          </th>
                          <th className='py-2'>AI Weather Forecast</th>
                          <th className='py-2'></th>
                        </tr>
                      </thead>
                      <tbody>
                        {routeOptions &&
                          routeOptions.map((option, index) => (
                            <tr key={index} className='border-t'>
                              <td className='py-2'>
                                {new Date(
                                  option.DepartureTime,
                                ).toLocaleString()}
                              </td>
                              <td className='py-2'>
                                {option.EstimatedTravelTime}
                              </td>
                              <td className='py-2'>{option.HazardsFraction}</td>
                              <td className='py-2'>
                                {option.IsAI ? (
                                  <FontAwesomeIcon
                                    icon={faCheckCircle}
                                    className='text-green-500'
                                    size='xl'
                                  />
                                ) : (
                                  <FontAwesomeIcon
                                    icon={faCircleXmark}
                                    className='text-red-500'
                                    size='xl'
                                  />
                                )}
                              </td>
                              <td>
                                <button
                                  type='button'
                                  className='inline-flex w-full justify-center rounded-md bg-indigo-600 py-2 text-base font-medium text-white shadow-sm hover:bg-indigo-700 sm:text-sm'
                                  onClick={() => {
                                    setSelectedOptionIsAI(false)
                                    setRouteOptionsOpen(false)
                                    setRouteSelected(index)
                                  }}
                                >
                                  Select This Departure Option
                                </button>
                              </td>
                            </tr>
                          ))}
                        {routeOptionsAI &&
                          routeOptionsAI.map((option, index) => (
                            <tr key={`ai-${index}`} className='border-t'>
                              <td className='py-2'>
                                {new Date(
                                  option.DepartureTime,
                                ).toLocaleString()}
                              </td>
                              <td className='py-2'>
                                {option.EstimatedTravelTime}
                              </td>
                              <td className='py-2'>{option.HazardsFraction}</td>
                              <td className='py-2'>
                                {option.IsAI ? (
                                  <FontAwesomeIcon
                                    icon={faCheckCircle}
                                    className='text-green-500'
                                    size='xl'
                                  />
                                ) : (
                                  <FontAwesomeIcon
                                    icon={faCircleXmark}
                                    className='text-red-500'
                                    size='xl'
                                  />
                                )}
                              </td>
                              <td>
                                <button
                                  type='button'
                                  className='inline-flex w-full justify-center rounded-md bg-green-600 py-2 text-base font-medium text-white shadow-sm hover:bg-green-700 sm:text-sm'
                                  onClick={() => {
                                    setSelectedOptionIsAI(true)
                                    setRouteOptionsOpen(false)
                                    setRouteSelected(index)
                                  }}
                                >
                                  Select AI Departure Option
                                </button>
                              </td>
                            </tr>
                          ))}
                      </tbody>
                      <tfoot>
                        <tr>
                          <td
                            colSpan={5}
                            className='py-2 text-xs text-gray-500'
                          >
                            AI Forecasts provided by ©{' '}
                            {new Date().getFullYear()} European Centre for
                            Medium-Range Weather Forecasts (ECMWF). Source:{' '}
                            <a
                              href='https://www.ecmwf.int'
                              className='text-blue-500 underline'
                            >
                              www.ecmwf.int
                            </a>
                            <br />
                            Licence Statement: This data is published under a{' '}
                            <a
                              href='https://creativecommons.org/licenses/by/4.0/'
                              className='text-blue-500 underline'
                            >
                              Creative Commons Attribution 4.0 International (CC
                              BY 4.0)
                            </a>
                            .
                            <br />
                            Disclaimer: ECMWF does not accept any liability
                            whatsoever for any error or omission in the data,
                            their availability, or for any loss or damage
                            arising from their use.
                          </td>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
                </div>
              </div>
              <div className='mt-5 sm:mt-6'></div>
            </div>
          </Transition.Child>
        </div>
      </Dialog>
    </Transition.Root>
  )
}
