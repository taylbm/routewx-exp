/** @type {import('next').NextConfig} */
const withPWA = require('next-pwa')
const withMDX = require('@next/mdx')({
  extension: /\.mdx?$/,
})
/** @type {import('next').NextConfig} */
const nextConfig = { output: 'export' }
const withBundleAnalyzer = require('@next/bundle-analyzer')({
  enabled: process.env.ANALYZE === 'true',
})

module.exports = withPWA(
  withMDX({
    images: {
      domains: [],
    },
    pwa: {
      dest: 'public',
    },
    pageExtensions: ['js', 'jsx', 'ts', 'tsx', 'md', 'mdx'],
  }),
  withBundleAnalyzer(nextConfig)
)
